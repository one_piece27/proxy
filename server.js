/**
* serve.js文件
* 
*/
var http = require('http'),
    url = require('url'),
    path=require('path'),
    fs=require('fs'),
    httpProxy = require('http-proxy'),
    mine=require('./mine').types;

//
// Create a proxy server with latency
//
// 需代理拦截的接口地址
var proxy = httpProxy.createProxyServer({
    //target: 'http://xptest.tianxialashou.com.cn',   // 接口地址
    target: 'https://api.douban.com',   // 豆瓣api测试接口地址
    // 下面的设置用于https
    // ssl: {
    //     key: fs.readFileSync('server_decrypt.key', 'utf8'),
    //     cert: fs.readFileSync('server.crt', 'utf8')
    // },
    // secure: false
});
// END 需代理拦截的接口地址

//
// Listen for the `proxyRes` event on `proxy`.
//
proxy.on('proxyRes', function (proxyRes, req, res) {
  console.log('RAW Response from the target', JSON.stringify(proxyRes.headers, true, 2));
});

//
// Listen for the `open` event on `proxy`.
//
proxy.on('open', function (proxySocket) {
  // listen for messages coming FROM the target here
  proxySocket.on('data', hybiParseAndLogMessage);
});

//
// Listen for the `close` event on `proxy`.
//
proxy.on('close', function (res, socket, head) {
  // view disconnected websocket connections
  console.log('Client disconnected');
});


var PORT = 3031;

//
// Create your server that makes an operation that waits a while
// and then proxies the request
//

var server = http.createServer(function (request, response) {
    var pathname = url.parse(request.url).pathname;
    //var realPath = path.join("main-pages", pathname); // 指定根目录
    var realPath = path.join("./", pathname);
    console.log(pathname);
    console.log(realPath);
    var ext = path.extname(realPath);
    ext = ext ? ext.slice(1) : 'unknown';

    // 判断如果接口访问有该关键字，则进行拦截，通过proxy转发
    if(pathname.match(/\/v2\//)) { // 豆瓣api接口
        delete request.headers.host;
        proxy.web(request, response);
        // response.write("alksjdfklll-----------------------------------?????");
        // response.end();
        return;
    }
    // END 判断如果接口访问有该关键字，则进行拦截，通过proxy转发

    fs.exists(realPath, function (exists) {
        if (!exists) {
            response.writeHead(404, {
                'Content-Type': 'text/plain'
            });

            response.write("This request URL " + pathname + " was not found on this server.");
            response.end();
        } else {
            fs.readFile(realPath, "binary", function (err, file) {
                if (err) {
                    response.writeHead(500, {
                        'Content-Type': 'text/plain'
                    });
                    response.end(err);
                } else {
                    var contentType = mine[ext] || "text/plain";
                    response.writeHead(200, {
                        'Content-Type': contentType
                    });
                    response.write(file, "binary");
                    response.end();
                }
            });
        }
    });
});
server.listen(PORT);
console.log("Server runing at port: " + PORT + ".");
// 3.mine.js文件
/**
* mine.js文件Created by BoyGXR on 2018-11-23.
* 该配置文件mine.js主要是服务器可以根据请求的文件不同，会使用相应mine类型的用json来存放一下常用的格式！用json来存放一下常用的格式
*/
exports.types = {
    "css": "text/css",
    "gif": "image/gif",
    "html": "text/html",
    "ico": "image/x-icon",
    "jpeg": "image/jpeg",
    "jpg": "image/jpeg",
    "js": "text/javascript",
    "json": "application/json",
    "pdf": "application/pdf",
    "png": "image/png",
    "svg": "image/svg+xml",
    "swf": "application/x-shockwave-flash",
    "tiff": "image/tiff",
    "txt": "text/plain",
    "wav": "audio/x-wav",
    "wma": "audio/x-ms-wma",
    "wmv": "video/x-ms-wmv",
    "xml": "text/xml",
    "woff": "application/x-woff",
    "woff2": "application/x-woff2",
    "tff": "application/x-font-truetype",
    "otf": "application/x-font-opentype",
    "eot": "application/vnd.ms-fontobject"
};